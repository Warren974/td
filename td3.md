## Lab 3: Testing other sensors

### Preliminaries

- Create a gitlab account
- Create a public repository in your gitlab account named: m4207
- Please leave a message in the issue: [ISSUE NUMBER 1](https://gitlab.com/m4207/td/issues/1), if not done yet.
- Every bit of codes, comments, documentation should be stored on your project
- At the end of each session, commit  (`git add`, `git commit`), tag `git tag TD3`) and push your work (`git push --tags`, `git push`). 

__If you find any errors, have any tips, tricks, advices, please open an issue__

__Put a picture on your gitlab profile, A REAL PICTURE, so that I can identify you__


### Important side notes

___If not done yet___

Since you are going to work in parallel in your group you have to implement some mechanims
to optimize your work. The most important part here is to implement a way to communicate.

You have also to let us know the composition of your group. To do that, drop a message with yout team member list
on [ISSUE NUMBER 2](https://gitlab.com/m4207/td/issues/2). If you create a specific repository for your project, 
invite me to be part of it (my login is: trazafin on gitlab and trazafin on github if you choose github). 


### Where are we so so far ? .

We have tested our board and some of its internal components. Now is the time to test the extension. 



#### EX. 1: Test the buzzer, the button and the LED


In this test you have to provide (copy/paste) a code that creates a beep and toogle the LED when the button is pressed. 

Upload your code and put the results of the serial output in a readme file. 


#### EX. 2: Test the sound sensors

In this test you should print on the serial port, the value of sound sensors. Explain your code 
with comments and the output value.

Upload your code and put the results of the serial output in a readme file. 




#### EX. 3: Test the external LED and the light detection. 

In this part of the lab we are going to test light Sensor. You objective is to  write
(or copy/paste) a code that changes the brightness of the LED depending on the value
provided by the light sensor (inversly should be good).

Upload your code and put the results of the serial output in a readme file. 





#### EX. 4: LCD and other sensors...  

Provide (copy/paste) a code that: 
Each time the button is pressed, the LCD Backlight should change the information it display 
in a round robin manner (choose only the sensors that can be used together in the shield):
- 3-axis Accelerometer
- Piezo Vibration
- Temperature
- Angle rotation
- Battery state and level
- ...
