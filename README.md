## This repository contains the documents related to labs

- TD1: Tools installation [here](https://gitlab.com/m4207/td/blob/master/td1.md)
- TD2: First code, unit testing [here](https://gitlab.com/m4207/td/blob/master/td2.md)
- TD3: Testing other sensors [here](https://gitlab.com/m4207/td/blob/master/td3.md)